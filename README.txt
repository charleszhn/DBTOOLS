第一步：本地配置好git环境
第二步：连接oschina中的ssh地址，并部署秘钥
第三步：clone仓库，下载代码，指定远程连接库
第四步：更新仓库
============================================
ssh-keygen -t rsa -C "xxx@gmail.com" 生成本地密钥

ssh -T git@git.oschina.net        /git@github.com

git config --global user.name "你的名字或昵称"
git config --global user.email "你的邮箱"

然后在你的需要初始化版本库的文件夹中执行
git init 
git remote add origin <你的项目地址> //注:项目地址形式为:http://git.oschina.net/xxx/xxx.git或者 git@git.oschina.net:xxx/xxx.git
这样就完成了一次版本你的初始化
如果你想克隆一个项目,只需要执行
git clone <项目地址>

git pull origin master
git touch init.txt //如果已经存在更改的文件,则这一步不是必须的
git add .
git commit -m "第一次提交"
git push origin master
============================================
本地创建仓库
mkdir BLOGS
cd BLOGS
git init
touch README.md
git add README.md
git commit -m "first commit"
git remote add origin https://git.oschina.net/xxx/repository.git
git push -u origin master
============================================
常用命令：
git remote rename origin oschina 更改仓库名
git remote add origin  仓库地址  添加仓库地址
git remote -v  查看现在连接的仓库
ssh-keygen -t rsa -C "xxxxx@xxxxx.com" 生成秘钥
ssh -T git@git.oschina.net 测试本地环境是否配置成功
git commit -a 说明  ESC :wq 添加注释并退出
git diff 比较版本差异
git config --list 查看配置信息
git config --global credential.helper store 设置记住密码

参考文档：
http://blog.csdn.net/zengraoli/article/details/10372221
http://git.oschina.net/progit/
